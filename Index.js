var express = require('express');
var chalk = require('chalk');
var app = express();
var router = express.Router();
var fs = require('fs');
var parser = require('body-parser');
var emrDb;

app.use(parser.json()); //support for json encoded bodies
app.use(parser.urlencoded({extend: true})); //support for url encoded bodies

//Reads the json file we're using to store data
fs.readFile('./emr.json', 'utf-8', (err, data) => {
    if(err){
        console.log(chalk.red(err));
    }
    else{
        emrDb = JSON.parse(data);
    }    

});

//Returns the entire json file and all of its entries
router.get('/emr', function(req, res) {
    res.json(emrDb);
});

//Uses the id parameter in the url to find the matching array id
router.get('/emr/:id', (req, res) => {
    res.json(emrDb[req.params.id]);
});

router.post('/emr', (req, res) => {
    //urlencoded is needed for the post method if using Post Man
    var body = req.body;

    var id = 0;

    //Increment our id variable for every array item in emrDb
    for (var i in emrDb) {

        id++;

    }    

    //Create a new entry
    emrDb[id] = {
        "id": id,
        "name": body.name,
        "description": body.description,
        "diagnosis": body.diagnosis,
        "cure": body.cure
    };


    res.status(201).json(emrDb.id);

});

app.use('/rest', router);

app.listen(process.env.PORT || 5000, function(err) {

    if (err) {

        console.log(chalk.red(err));

    } else {

        console.log(chalk.blue('Magic Happens on Port 69'));

    }

});